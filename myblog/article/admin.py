from django.contrib import admin

from article.models import Article, Category, Comment


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'slug', 'created_at')
    list_filter = ('created_at', 'category')
    list_display_links = ('slug',)
    list_editable = ('title',)
    list_per_page = 2
    list_max_show_all = True
    # list_max_show_all = 30
    fields = [('title', 'slug', 'cover_image_url'),
              ('publish_time', 'visibily'),
              'content', 'category', 'summary', 'image_preview']
    autocomplete_fields = ('category',)
    prepopulated_fields = {'slug': ('title',)}
    readonly_fields = ('id', 'image_preview')
    search_fields = ('title', 'category__name')

    def image_preview(self, obj):
        return obj.image_preview

    image_preview.short_description = "image de couverture"
    image_preview.allow_tags = True


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'image_url')
    list_filter = ('name',)
    #    list_display_links = ('name',)
    #   list_editable = ('name',)
    list_per_page = 2
    list_max_show_all = True
    # list_max_show_all = 30
    fields = ['name', 'image_url']
    search_fields = ('name',)


class CommentAdmin(admin.ModelAdmin):
    list_display = ('author_name', 'content', 'created_at')
    list_display = ('content',)
    list_filter = ('created_at',)
    autocomplete_fields = ('article',)
    search_fields = ('author_name',)

    search_fields = ('author_name',)
    fields = [('author_name',), 'content']
    list_per_page = 2
    list_max_show_all = True


admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Comment, CommentAdmin)
