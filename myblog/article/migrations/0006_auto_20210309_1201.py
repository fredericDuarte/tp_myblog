# Generated by Django 3.1.7 on 2021-03-09 11:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0005_auto_20210309_1145'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='visibilty',
            new_name='visibily',
        ),
        migrations.AddField(
            model_name='article',
            name='file',
            field=models.FileField(blank=True, help_text='ajouter un fichier', null=True, upload_to='files/'),
        ),
        migrations.AlterField(
            model_name='article',
            name='cover_image_url',
            field=models.ImageField(default='', height_field='200', help_text='Enter an image url for the article', null=True, upload_to='images/', width_field='200'),
        ),
    ]
