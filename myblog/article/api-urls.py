from django.urls import path, include
from . import views
from rest_framework import routers


# créer router
router = routers.DefaultRouter()
router.register(r'articles', views.ArticleViewSet)
router.register(r'categories', views.CategoryViewSet)
router.register(r'comments', views.CommentViewSet)

urlpatterns = [
    path('', include(router.urls),name="api"),
]
