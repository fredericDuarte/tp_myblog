from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'articles', views.ArticleViewSet)
router.register(r'categories', views.CategoryViewSet)
router.register(r'comments', views.CommentViewSet)


urlpatterns = [
    path('', views.index, name='index'),
    path('categories/', views.category_index, name='categories'),
    path('comment/', views.comment_index, name='comment'),

    path('list/', views.ArticleListView.as_view(), name='uneliste'),
    path('<int:pk>', views.ArticleDetailView.as_view(), name='article-detail'),

    path('api/', include(router.urls)),
    path('api/list', include(router.urls)),

    # path('<str:slug>', views.ArticleDetailView.as_view(), name='article-detail'),

]
